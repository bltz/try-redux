import axios from 'axios'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
export const UPDATE_USER = 'users:updateUser';
export const SHOW_ERROR = 'users:showError';
export const SHOW_REQUEST = 'users:showRequest'

export function updateUser(data){
  return {
    type : UPDATE_USER,
    payload : {
      user : data
    }
  }
}

export function showError(){
  return {
    type : SHOW_ERROR,
    payload : {
      user : 'ERROR !!!'
    }
  }
}

export function showRequest(newUser){
  return {
    type : SHOW_REQUEST,
    payload : {
      user : newUser
    }
  }
}

export function apiRequest(){
  return dispatch => {
    // dispatch(requestMade());
    console.log(showLoading(),'????')
    dispatch(showLoading())
    axios.get('http://localhost:3000/query/search?q=angsa').then( response => {
      console.log('SUCCESS',response.data)
      dispatch(updateUser(response.data[0].writeBy.userInfoID.fullName))
      dispatch(hideLoading())
      // setTimeout(() => {
      //   dispatch(hideLoading())
      // }, 1500);
    }).catch( error => {
      console.log('ERROR <<< from action users')
      dispatch(showError())
      setTimeout(() => {
        dispatch(hideLoading())
      }, 1500);
    })
  }
}