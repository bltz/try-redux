import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';

// // import { AppContainer } from 'react-hot-loader';

// import thunk from 'redux-thunk'
// import { applyMiddleware, compose, combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import store from './store'
// import productsReducer from './reducers/productsReducer'
// import userReducer from './reducers/userReducer'

// // single reducer
// // function reducer(state,action) {
// //   if(action.type === 'changeState'){
// //     return action.payload.newState
// //   }
// // }

// // const store = createStore(reducer);

// // const action = {
// //   type : 'changeState',
// //   payload : {
// //     newState : 'New State Action'
// //   }
// // }

// // store.dispatch(action)
// // console.log(store.getState())

// // combine reducer
// // function productsReducer(state = [], action){
// //   return state;
// // }

// // function userReducer(state = '', {type, payload}){
// //   switch (type) {
// //     case 'updateUser':
// //       return payload.user
// //     default :
// //     return state
// //   }
// // }

// const allReducers = combineReducers({
//   products : productsReducer,
//   user : userReducer
// })

// const allStoreEnhancers = compose(
//   applyMiddleware(thunk),
//   window.devToolsExtension && window.devToolsExtension()
// );

// const store = createStore(
//   allReducers,
//   {
//     products : [{name : 'iphone'}],
//     user : 'change name'
//   },
//   allStoreEnhancers
// )
// // console.log(store.getState())
// // // actions
// // const updateUserAction = {
// //   type : 'updateUser',
// //   payload : {
// //     user : 'Reza aditya'
// //   }
// // }

// // store.dispatch(updateUserAction)

ReactDOM.render(
  // <AppContainer>
    <Provider store={store}>
      <App randomName='whatever'/>
    </Provider>
  // </AppContainer>
, document.getElementById('root'));
registerServiceWorker();


// // Hot Module Replacement API
// if (module.hot) {
//   module.hot.accept()
//   module.hot.accept('./App', () => {
//     const NextApp = require('./App').default;
//     ReactDOM.render(
//       <AppContainer>
//         <NextApp/>
//       </AppContainer>
//       ,
//       document.getElementById('root')
//     );
//   });
// }
