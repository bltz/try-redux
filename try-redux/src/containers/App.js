import React, { Component } from 'react';
import '../App.css';
import logo from '../logo.svg';
import Header from '../components/Header'

// import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateUser, apiRequest } from '../actions/userAction'

class App extends Component {
  constructor(props){
    super(props)
    this.onUpdateUserByClick = this.onUpdateUserByClick.bind(this)
    this.onUpdateUserByInput = this.onUpdateUserByInput.bind(this)
  }

  componentDidMount(){
    setTimeout(() => {
      this.props.onApiRequest();
    }, 2000);
  }

  onUpdateUserByClick(){
    if(this.props.user === 'Fildha Indah'){
      this.props.onUpdateUserByClick('Reza Aditya')
    }else{
      this.props.onUpdateUserByClick('Fildha Indah')
    }
  }

  onUpdateUserByInput(event){
    this.props.onUpdateUserByInput(event.target.value)
  }

  render() {
    
    return (
      <div className="App">
        <Header logo={logo} products={this.props.products}/>
        {/* {console.log(this.props.user,'>>>>>>')} */}
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <button onClick={this.onUpdateUserByClick}>update user :</button>
        <p style={{fontSize:'xx-large',fontWeight:'bold'}}>{this.props.userRandom}</p>
        <br/><br/>
        <input onChange={this.onUpdateUserByInput} placeholder={this.props.user} value={this.props.user}/>
      </div>
    );
  }
}

// const mapStateToProps = state => ({
//   products : state.products,
//   user : state.user
// })

const mapStateToProps = (state,props) => {
  return {
    products : state.products,
    user : state.user,
    userRandom : `${state.user} ${props.randomName}`
  }
}

// const mapActionsToProps = (dispatch,props) => {
//   // console.log(props,'<<<<< props from store')
//   return bindActionCreators({
//     onUpdateUserByClick : updateUser,
//     onUpdateUserByInput : updateUser
//   }, dispatch)
// }
const mapActionsToProps = {
  onUpdateUserByClick : updateUser,
  onUpdateUserByInput : updateUser,
  onApiRequest : apiRequest
}
// const mergeProps = (propsFromState,propsFromDispatch,ownProps) => {
//   console.log(propsFromState,'<< propsFromState', propsFromDispatch, '<< propsFromDispatch', ownProps,'<< ownProps')
//   return{}
// }

export default connect(
  mapStateToProps,
  mapActionsToProps,
  // mergeProps
)(App);
