// import { AppContainer } from 'react-hot-loader';

import thunk from 'redux-thunk'
import promiseMiddleware from 'redux-promise-middleware'
import { createLogger } from 'redux-logger'
// import { loadingBarMiddleware } from 'react-redux-loading-bar'
import { loadingBarReducer } from 'react-redux-loading-bar'

import { applyMiddleware, compose, combineReducers, createStore } from 'redux';
// import { Provider } from 'react-redux';


import productsReducer from './reducers/productsReducer'
import userReducer from './reducers/userReducer'


// single reducer
// function reducer(state,action) {
//   if(action.type === 'changeState'){
//     return action.payload.newState
//   }
// }

// const store = createStore(reducer);

// const action = {
//   type : 'changeState',
//   payload : {
//     newState : 'New State Action'
//   }
// }

// store.dispatch(action)
// console.log(store.getState())

// combine reducer
// function productsReducer(state = [], action){
//   return state;
// }

// function userReducer(state = '', {type, payload}){
//   switch (type) {
//     case 'updateUser':
//       return payload.user
//     default :
//     return state
//   }
// }

const allReducers = combineReducers({
  products : productsReducer,
  user : userReducer,
  loadingBar: loadingBarReducer,
})

// const allStoreEnhancers = compose(
//   applyMiddleware(
//     thunk,
//     promiseMiddleware(), // resolves promises
//     loadingBarMiddleware(), // manages loading bar
//     createLogger(), // log actions in console
//   ),
//   window.devToolsExtension && window.devToolsExtension()
// )(createStore)

const allStoreEnhancers = compose(
  applyMiddleware(
    thunk,
    promiseMiddleware(), // resolves promises
    // loadingBarMiddleware(), // manages loading bar
    createLogger(), // log actions in console
  ),
  window.devToolsExtension && window.devToolsExtension()
)

const store = createStore(
  allReducers,
  {
    products : [],
    user : 'change name'
  },
  allStoreEnhancers
)

// const store = allStoreEnhancers(allReducers)

// console.log(store.getState())
// // actions
// const updateUserAction = {
//   type : 'updateUser',
//   payload : {
//     user : 'Reza aditya'
//   }
// }

// store.dispatch(updateUserAction)

export default store