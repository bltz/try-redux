import React from 'react'
import LoadingBar from 'react-redux-loading-bar'

const Header = (props) => {
    return (
      <div>
        <LoadingBar showFastActions style={{ backgroundColor: 'red', height: '2px' }} />
        <header className="App-header">
        
          <img src={props.logo} className="App-logo" alt="logo" />
          <h1 className="App-title">WelCome to React-Redux</h1>
        </header>
      </div>
    )
}

export default Header
